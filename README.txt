CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers

INTRODUCTION
------------

The COD mobile module works with drupal's 'Conference Organizing Distribution'
and will generate a json file with session and related data such as speakers,
tracks, room etc. The json file will generate automatically during drupal cron
run or you can generate it manually by accessing the
link example.com/cod-mobile/generate-json.

The genrated json file will be placed inside the public files folder of the
drupal installation eg: example.com/sites/default/files/sessions.json.
This can be later used as a data provider for mobile applicatons.

There is a version attached to the json file which can be accessed through
the link example.com/cod-mobile/json-version. The version changes when the
json file is updated with a set of new data. This is a useful feature for
mobile applications to check if there any updations in json file data.

This module can be used as a backend drupal module to serve data for
the mobile application project https://github.com/zyxware/drupalcon-mobileapp.

REQUIREMENTS
------------

This module requires the following distribution of drupal:
 * Conference Organizing Distribution (https://www.drupal.org/project/cod)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

MAINTAINERS
-----------

This project has been sponsored by:
 * Zyxware Technologies - https://www.drupal.org/u/zyxware
