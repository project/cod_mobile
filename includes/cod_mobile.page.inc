<?php
/**
 * @file
 * Contains page call back of menu defined in cod_custom.module.
 */

/**
 * Callback function of menu generate-json.
 */
function cod_custom_json_output() {
  $node_revions_count = _cod_get_node_revision_count();
  $json_revision_count = _cod_get_json_revision_count();
  if (variable_get('generate_json') == TRUE || $node_revions_count != $json_revision_count) {
    _cod_mobile_generate_json_file();
  }
  return 'Created';
}

/**
 * Callback function of menu json-version.
 */
function cod_custom_json_version() {
  $json_version = _cod_mobile_get_json_version();
  $json = array('version' => $json_version);
  drupal_json_output($json);
}

/**
 * Webservice callback.
 * Authorize the specified user.
 */
function cod_user_authorization($post_data) {
  $uname = $pass = $user_authenticate = "";
  if (!empty($_REQUEST['uname'])) {
    $uname = $_REQUEST['uname'];
  }
  if (!empty($_REQUEST['pass'])) {
    $pass = $_REQUEST['pass'];
  }
  if($uname != "" && $pass != "") {
    $uid = user_authenticate($uname, $pass);
    $user_array = array('uid' => $uid);
  }
  drupal_json_output($user_array);
}

/**
 * Webservice callback.
 * Get session ratings.
 */
function cod_user_session_rating() {
  if (module_exists('fivestar')) {
    if (is_numeric($_REQUEST['user_id'])) {
      $uid = $_REQUEST['user_id'];
    }
    else {
      drupal_json_output(array(
        'failed' => 'User ID should be numeric',
      ));
      exit;
    }
    if (is_numeric($_REQUEST['fivestar_value'])) {
      $fivestar_value = $_REQUEST['fivestar_value'];
    }
    else {
      drupal_json_output(array(
        'failed' => 'Star rating value should be numeric',
      ));
      exit;
    }
    if (is_numeric($_REQUEST['session_id'])) {
      $session_id = $_REQUEST['session_id'];
    }
    else {
      drupal_json_output(array(
        'failed' => 'Session ID should be numeric',
      ));
      exit;
    }
    $entity_type_name = 'node';
    $entity = node_load($session_id);
    $entity->field_session_ranking[$entity->language][0]['rating'] = $fivestar_value;
    _fivestar_cast_vote('node', $session_id, $fivestar_value, 'vote', $uid);
    node_save($entity);
    drupal_json_output(array(
      'success' => 'true',
    ));
    exit;
  }
}

/**
 * Webservice callback.
 * Get session reviews.
 */
function cod_user_session_reviews() {
  if (module_exists('comment')) {
    if (is_numeric($_REQUEST['user_id'])) {
      $uid = $_REQUEST['user_id'];
    }
    else {
      drupal_json_output(array(
        'failed' => 'User ID should be numeric',
      ));
      exit;
    }
    if (!empty($_REQUEST['comment_title'])) {
      $comment_title = $_REQUEST['comment_title'];
    }
    else {
      $comment_title = NULL;
    }
    if (!empty($_REQUEST['comment_body'])) {
      $comment_body = $_REQUEST['comment_body'];
    }
    else {
      drupal_json_output(array(
        'failed' => 'Comment Body filed is NULL',
      ));
      exit;
    }
    if (!empty($_REQUEST['user_name'])) {
      $user_name = $_REQUEST['user_name'];
    }
    else {
      drupal_json_output(array(
        'failed' => 'Username filed is NULL',
      ));
      exit;
    }
    if (is_numeric($_REQUEST['session_id'])) {
      $session_id = $_REQUEST['session_id'];
    }
    else {
      drupal_json_output(array(
        'failed' => 'Session ID should be numeric',
      ));
      exit;
    }
    $comment = (object) array(
      'nid' => $session_id,
      'name' => $user_name,
      'cid' => NULL,
      'node_type' => 'comment_node_session',
      'pid' => 0,
      'uid' => $uid,
      'created' => time(),
      'changed' => time(),
      'status' => COMMENT_PUBLISHED,
      'subject' => $comment_title,
      'hostname' => ip_address(),
      'language' => LANGUAGE_NONE,
      'comment_body' => array(LANGUAGE_NONE => array(0 => array ('value' => $comment_body,'format' => 'full_html'))),
    );
    comment_save($comment);
    drupal_json_output(array(
      'success' => 'true',
    ));
    exit;
  }
}
